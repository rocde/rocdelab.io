---
title: 关于
date: 2021-01-31 13:20:36
updated: 2021-03-02 15:52:31
type: "about"
comments: false
---

> You got a dream, you gotta protect it. People can't do something themselves, they wanna tell you you can't do it. If you want something, go get it. Period.  --The Pursuit of Happyness

#### 关于本人

- SC
- 本科在读

#### 关于站点

- 使用 [Hexo](https://hexo.io/zh-cn/) 建立
- 托管于 [GitLab](https://gitlab.com/) 
- 使用主题 [NexT](https://github.com/theme-next/hexo-theme-next) 
- 评论支持 [Valine](https://valine.js.org/)、[LeanCloud](https://www.leancloud.cn/) 
- 同时部署于 [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) 和 [Netlify](https://www.netlify.com/) 

