---
title: Typora 嵌入 Latex
date: 2021-02-19 15:41:19
updated: 2021-02-19 15:41:24
type: 
mathjax: false
tags: 
categories: 
comments: 
---

# Typora 嵌入 Latex

## 上下标

### 上标

`$y=x^2$`
$$
y=x^2
$$

### 下标

`$CuSO_4·5H_20$`
$$
CuSO_4·5H_20
$$

### 上下结合

`$S_n = na_1 (q=1)`
$$
S_n = na_1 (q=1)
$$

`$S_n = \frac{a_1 (1 - q^n)}{1-q} (q\neq1)$`

$$
S_n = \frac{a_1 (1 - q^n)}{1-q} (q\neq1)
$$

## 汉字、字体与格式

### 汉字形式

`$V_{\mbox{升}}$`

$$
V_{\mbox{升}}
$$

### 字体控制

`$\displaystyle \frac{x+y}{y+z}$`
$$
\displaystyle \frac{x+y}{y+z}
$$

`$\frac{x+y}{y+z}$`
$$
\frac{x+y}{y+z}
$$

### 下划线

`$\underline{x+y}$`
$$
\underline{x+y}
$$

### 标签

`$\tag{69}$`
$$
\tag{69}
$$

### 上大括号

`$\overbrace{a+b+c}^{2.0}$`
$$
\overbrace{a+b+c}^{2.0}
$$

### 下大括号

`$a+\underbrace{b+c}_{1.0}+d$`
$$
a+\underbrace{b+c}_{1.0}+d
$$

### 上位符号

`$\vec{x}\stackrel{\mathrm{def}}{=}{x_1,\dots,x_n}$`
$$
\vec{x}\stackrel{\mathrm{def}}{=}{x_1,\dots,x_n}
$$

## 占位符

### 两个 quad 空格

`$x\qquad y$`
$$
x\qquad y
$$

### quad 空格

`$x\quad y$`
$$
x\quad y
$$

### 大空格

`$x \ y$`
$$
x \ y
$$

### 中空格

`$x \: y$`
$$
x \: y
$$

### 小空格

`$x \, y$`
$$
x \, y
$$

### 贴紧

`$x \! y$`
$$
x \! y
$$

### 无空格

`$xy$`
$$
xy
$$

## 四则运算

### 加减运算

`$x \pm y = z$`
$$
x \pm y = z
$$

### 减加运算

`$x \mp y = z$`
$$
x \mp y = z
$$

### 叉乘

`$x \times y = z$`
$$
x \times y = z
$$

### 点乘

`$x \cdot y = z$`
$$
x \cdot y = z
$$

### 星乘

`$x \ast y = z$`
$$
x \ast y = z
$$

### 除号

`$x \div y = z$`
$$
x \div y = z
$$

### 斜杠除

`$x / y = z$`
$$
x / y = z
$$

### 分式

`$\frac{x+y}{y+z}$`
$$
\frac{x+y}{y+z}
$$

`${x+y}\over{y+z}$`
$$
{x+y}\over{y+z}
$$

### 绝对值

`$y = |x|$`
$$
y = |x|
$$

## 高级运算

### 平均数

`$\overline{xyz}$`
$$
\overline{xyz}
$$

### 开方

`$\sqrt x$`
$$
\sqrt x
$$

`$\sqrt[3]{x+y}$`
$$
\sqrt[3]{x+y}
$$

### 对数

`$\log_5{x}$`
$$
\log_5{x}
$$

### 极限

`$\lim^{x \to \infty}_{y \to 0}{\frac{x}{y}}$`
$$
\lim^{x \to \infty}_{y \to 0}{\frac{x}{y}}
$$

`$\displaystyle \lim^{x \to \infty}_{y \to 0}{\frac{x}{y}}$`
$$
\displaystyle \lim^{x \to \infty}_{y \to 0}{\frac{x}{y}}
$$

### 求和

`$\sum^{x \to -\infty}_{y \to 0}{\frac{x}{y}}$`
$$
\sum^{x \to -\infty}_{y \to 0}{\frac{x}{y}}
$$

### 积分

`$\int^{\infty}_{0}{xdx}$`
$$
\int^{\infty}_{0}{xdx}
$$

`$\displaystyle \int^{\infty}_{0}{xdx}$`
$$
\displaystyle \int^{\infty}_{0}{xdx}
$$

### 微分

`$\frac{\partial x}{\partial y}$`
$$
\frac{\partial x}{\partial y}
$$

`$\displaystyle \frac{\partial x}{\partial y}$`
$$
\displaystyle \frac{\partial x}{\partial y}
$$

### 矩阵

```
$$\left[ \begin{matrix} 1 & 2  & \cdots & 5 & 6 & \cdots & 9 & 10 \\ \vdots & \vdots & \cdots & \vdots & \vdots & \cdots & \cdots & \ddots \\ a & b & \cdots & e & f & \cdots & i & j \end{matrix} \right]$$
```

$$
\left[ \begin{matrix} 1 & 2  & \cdots & 5 & 6 & \cdots & 9 & 10 \\ \vdots & \vdots & \cdots & \vdots & \vdots & \cdots & \cdots & \ddots \\ a & b & \cdots & e & f & \cdots & i & j \end{matrix} \right]
$$

## 逻辑运算

### 小于等于

`$x+y \leq z$`
$$
x+y \leq z
$$

### 大于等于

`$x+y \geq z$`
$$
x+y \geq z
$$

### 不等于

`$x \neq y$`
$$
x \neq y
$$

### 不大于等于

`$x \ngeq y$`
$$
x \ngeq y
$$

`$x \not\geq y$`
$$
x \not\geq y
$$

### 不小于等于

`$x \nleq y$`
$$
x \nleq y
$$

`$x \not\leq y$`
$$
x \not\leq y
$$

### 约等于

`$\frac{1}{3} \approx 0.3$`
$$
\frac{1}{3} \approx 0.3
$$

`$\displaystyle \frac{1}{3} \approx 0.3$`
$$
\displaystyle \frac{1}{3} \approx 0.3
$$

### 恒等于

`$x + y \equiv z$`
$$
x + y \equiv z
$$

## 集合运算

### 属于

`$x \in A$`
$$
x \in A
$$

### 不属于

`$x \notin A, y \not \in B$`
$$
x \notin A, y \not \in B
$$

### 子集

`$A \subset B, A \supset B$`
$$
A \subset B, A \supset B
$$

### 非子集

`$A \not\subset B, A \not\supset B$`
$$
A \not\subset B, A \not\supset B
$$

### 真子集

`$A \subseteq B, A \supseteq B$`
$$
A \subseteq B, A \supseteq B
$$

### 非真子集

`$A \subsetneq B, A \supsetneq B$`
$$
A \subsetneq B, A \supsetneq B
$$

### 交集

`$A \cap B$`
$$
A \cap B
$$

### 并集

`$A \cup B$`
$$
A \cup B
$$

### 差集

`$A \setminus B$`
$$
A \setminus B
$$

### 同或运算

`$A \bigodot B$`
$$
A \bigodot B
$$

### 同与运算

`$A \bigotimes B$`
$$
A \bigotimes B
$$

### 常用数集

`\mathbb{数集字母}`

### 空集

`$\empty, \emptyset$`
$$
\empty, \emptyset
$$

## 数学符号

### 无穷

`$\infty, +\infty, -\infty$`
$$
\infty, +\infty, -\infty
$$

### 虚数

`$\imath, \jmath$`
$$
\imath, \jmath
$$

### 向量符号

`$\vec{AC} = \vec{AB} + \vec{BC}$`
$$
\vec{AC} = \vec{AB} + \vec{BC}
$$

### 导数

`$\dot{y}, \ddot{y}, \dddot{y}$`
$$
\dot{y}, \ddot{y}, \dddot{y}
$$

### 箭头

#### 上箭头

`\uparrow`

$$
\uparrow
$$

#### 双线上箭头

`\Uparrow`
$$
\Uparrow
$$

#### 下箭头

`\downarrow`
$$
\downarrow
$$

#### 双线下箭头

`\Downarrow`
$$
\Downarrow
$$

#### 左箭头

`\leftarrow`
$$
\leftarrow
$$

#### 双线左箭头

`\Leftarrow`
$$
\Leftarrow
$$

#### 右箭头

`\rightarrow`
$$
\rightarrow
$$

#### 双线右箭头

`\Rightarrow`
$$
\Rightarrow
$$

### 省略号

#### 低端对齐

`$\ldots$`
$$
\ldots
$$

#### 中线对齐

`$\cdots$`
$$
\cdots
$$

#### 垂直对齐

`$\vdots$`
$$
\vdots
$$

#### 斜对齐

`$\ddots$`
$$
\ddots
$$

### 其他数学符号

`$\bar{a}$`
$$
\bar{a}
$$

`$\acute{a}$`
$$
\acute{a}
$$

`$\check{a}$`
$$
\check{a}
$$

`$\grave{a}$`
$$
\grave{a}
$$

`$\hat{a}$`
$$

$$
`$\breve{a}$`
$$

\breve{a}
$$

`$\tilde{a}$`
$$
\tilde{a}
$$

`$\mathring{a}$`
$$
\mathring{a}
$$

## 希腊字母

| 大写字母 | 效果         | 代码           | 小写字母 | 效果         | 代码           |
|:----:|:----------:|:------------:|:----:|:----------:|:------------:|
| A    | $\Alpha$   | `$\Alpha$`   | α    | $\alpha$   | `$\alpha$`   |
| B    | $\Beta$    | `$\Beta$`    | β    | $\beta$    | `$\beta$`    |
| Γ    | $\Gamma$   | `$\Gamma$`   | γ    | $\gamma$   | `$\gamma$`   |
| Δ    | $\Delta$   | `$\Delta$`   | δ    | $\delta$   | `$\delta$`   |
| Ε    | $\Epsilon$ | `$\Epsilon$` | ε    | $\epsilon$ | `$\epsilon$` |
| Ζ    | $\Zeta$    | `$\Zeta$`    | ζ    | $\zeta$    | `$\zeta$`    |
| Η    | $\Eta$     | `$\Eta$`     | η    | $\eta$     | `$\eta$`     |
| Θ    | $\Theta$   | `$\Theta$`   | θ    | $\theta$   | `$\theta$`   |
| Ι    | $\Iota$    | `$\Iota$`    | ι    | $\iota$    | `$\iota$`    |
| Κ    | $\Kappa$   | `$\Kappa$`   | κ    | $\kappa$   | `$\kappa$`   |
| Λ    | $\Lambda$  | `$\Lambda$`  | λ    | $\lambda$  | `$\lambda$`  |
| Μ    | $\Mu$      | `$\Mu$`      | μ    | $\mu$      | `$\mu$`      |
| Ν    | $\Nu$      | `$\Nu$`      | ν    | $\nu$      | `$\nu$`      |
| Ξ    | $\Xi$      | `$\Xi$`      | ξ    | $\xi$      | `$\xi$`      |
| Ο    | $\Omicron$ | `$\Omicron$` | ο    | $\omicron$ | `$\omicron$` |
| Π    | $\Pi $     | `$\Pi $`     | π    | $\pi$      | `$\pi$`      |
| Ρ    | $\Rho$     | `$\Rho$`     | ρ    | $\rho$     | `$\rho$`     |
| Σ    | $\Sigma$   | `$\Sigma$`   | σ    | $\sigma$   | `$\sigma$`   |
| Τ    | $\Tau$     | `$\Tau$`     | τ    | $\tau$     | `$\tau$`     |
| Υ    | $\Upsilon$ | `$\Upsilon$` | υ    | $\upsilon$ | `$\upsilon$` |
| Φ    | $\Phi$     | `$\Phi$`     | φ    | $\phi$     | `$\phi$`     |
| Χ    | $\Chi$     | `$\Chi$`     | χ    | $\chi$     | `$\chi$`     |
| Ψ    | $\Psi$     | `$\Psi$`     | ψ    | $\psi$     | `$\psi$`     |
| Ω    | $\Omega$   | `$\Omega$`   | ω    | $\omega$   | `$\omega$`   |
