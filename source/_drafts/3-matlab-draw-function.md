---
title: Matlab 图形绘制函数
date: 2021-02-18 12:09:22
updated: 2021-02-18 12:09:22
type:
mathjax: false
tags:
- matlab
categories:
- matlab
comments: true
---



# 图形绘制函数

MATLAB 不仅提供了大量解决代数问题的函数，还提供了大量的图形绘制函数，这里汇总一下图形的绘制，以便以后查阅。

<!--more-->

## 二维曲线绘制

在 MATLAB 中使用 $ezplot$ 函数进行二维曲线的绘制，该函数可以绘制显函数图形、隐函数图形和参数方程图形，调用格式如下。

1. $ezplot(f, [min,max])$：用于绘制显函数 $y = f(x) $的图像，函数区间为 $[min, max]$。
2. $ezplot(f,[x_{min}, x_{max}, y_{min}, y_{max}])$：用于绘制隐函数 $f(x, y) =  0$ 的图形，函数区间为 $x_{min}<x<x_{max}$、$y_{min}<y<y_{max}$。  
3. $ezplot(x, y, [t_{min}, t_{max}])$：用于绘制参数方程 $x = x(t)$、$y = y(t)$ 的图形，函数区间为 $[t_{min},t_{max}]$。

例：

- 绘制显函数 $cosx$ 的二维曲线；

```matlab
syms x;
f = cos(x);
ezplot(f);
xlabel('x');
ylabel('y');
title('cosx 函数图像')
```

运行结果：

![](https://gitee.com/RocDe/picture/raw/master/img/20210218211739.png)

- 绘制隐函数 $f(x, y) = x^2sin(x+y^2)+y^2e^x+6cos(x^2+y)=0$ 的二维曲线。

```matlab
syms x;
syms y;
f=x^2*sin(x+y^2)+y^2*exp(x)+6*cos(x^2+y);
T=ezplot(f);
set(T,'Color','k')
xlabel('x');
ylabel('y');
title('隐函数图像')
```

运行结果：

![](https://gitee.com/RocDe/picture/raw/master/img/20210218212221.png)



## 三维曲线绘制

在 MATLAB 中使用 $ezplot$ 函数进行三维曲线的绘制，调用格式如下。

1. $ezplot3(x,t,z)$：绘制参数方程 $x=x(t)$、$y=y(t)$、$z=z(t)$ 的三维曲线图，$t$ 的取值范围为 $[0,2]$。
2. $ezplot3(x,y,z,[t_{min},t_{max}])$：绘制参数方程 $x=x(t)$、$y=y(t)$、$z=z(t)$ 的三维曲线图，$t$ 的取值范围为 $[t_{min},t_{max}]$。
3. $ezplot3(x,y,z,[t_{min},t_{max}],'animate')$ ：绘制参数方程 $x=x(t)$、$y=y(t)$、$z=z(t)$ 的空间曲线的动态轨迹，$t$ 的取值范围为 $[t_{min},t_{max}]$。

例：

- 绘制 $x=sint$、$y=cost$、$z=t$ 的空间曲线动态轨迹，$t\in[0,10\pi]$。

```matlab
syms t;
x = sin(t);
y = cos(t);
z = t;
ezplot3(x,y,z,[0,10*pi],'animate');
```

运行结果：

![](https://gitee.com/RocDe/picture/raw/master/img/20210218212612.png)



## 等值线绘制

在 MATLAB 中使用 $ezcontour$ 函数和 $ezcontourf$ 函数进行等值线的绘制，其调用格式如下。

1. $ezcontour(f)$：绘制二元函数 $f(x,y)$ 在默认区域的等值线。
2. $ezcontour(f,[x_{min},x_{max}],[y_{min},y_{max}])$：绘制二元函数 $f(x,y)$ 在指定区域的等值线，其中 $x$ 和 $y$ 的区间分别为 $[x_{min},x_{max}]$和 $[y_{min},y_{max}]$。
3. $ezcountour(f,[x_{min},x_{max}],[y_{min},y_{max}],n)$：绘制等值线图，并指定等值线的条数为 $n$，其中 $x$ 和 $y$ 的区间分别为 $[x_{min},x_{max}]$和 $[y_{min},y_{max}]$。
4. $ezcontourf(f)$：绘制二元函数 $f(x,y)$ 在默认区域的带有3填充颜色的等值线。
5. $ezcontourf(f,[x_{min},x_{max}],[y_{min},y_{max}])$：绘制二元函数 $f(x,y)$ 在指定区域内的带有填充颜色的等值线，其中 $x$ 和 $y$ 的区间分别为 $[x_{min},x_{max}]$和 $[y_{min},y_{max}]$。
6. $ezcontourf(f,[x_{min},x_{max}],[y_{min},y_{max}],n)$：绘制带有填充颜色的等值线，并指定等值线的条数为 $n$ ，其中 $x$ 和 $y$ 的区间分别为 $[x_{min},x_{max}]$和 $[y_{min},y_{max}]$。

$ezcontour$ 函数和 $ezcontourf$ 函数的使用方法是相似的，但 $ezcontour$ 函数只能实现等值线的绘制，而 $ezcontourf$ 函数可以实现带有填充颜色的等值线绘制。

例：

- 绘制 $xsint$ 在区间 $[-5,5]$ 内的等值线和带填充色的等值线。

```matlab
syms x;
syms t;
f=x*sin(t);
ezcontour(f,[-5,5]);
xlabel('x');
ylabel('y');
title('无填充色等值线')
```

运行结果：

![](https://gitee.com/RocDe/picture/raw/master/img/20210218213229.png)

```matlab
syms x;
syms y;
f=x*sin(t);
ezcontourf(f,[-5,5]);
xlabel('x');
ylabel('y');
title('有填充色等值线')
```

运行结果：

![](https://gitee.com/RocDe/picture/raw/master/img/20210218213543.png)



## 表面图绘制

在 MATLAB 中使用 $ezsurf$ 函数和 $ezsurfc$ 函数进行表明图的绘制，其调用格式如下。

1. $ezsurf(f)$：绘制二元函数 $f(x,y)$ 在默认区域的表明图。
2. $ezsurf(f,[x_{min},x_{max}],[y_{min},y_{max}])$：绘制二元函数 $f(x,y)$ 在指定区域的表面图，其中 $x$ 和 $y$ 的区间分别为 $[x_{min},x_{max}]$ 和 $[y_{min},y_{max}]$。
3. $ezsurf(x,y,z)$：在默认区域内绘制三维参数方程的表明图。
4. $ezsurf(x,y,z,[s_{min},s_{max},t_{min},t_{max}])$ 或 $ezsurf(x,y,z,[min,max])$：在指定区域内绘制三维参数方程的表面图。
5. $ezsurfc(f)$ 绘制二元函数 $f(x,y)$ 在默认区域内带有等值线的表面图。
6. $ezsurfc(f,[x_{min},x_{max}],[y_{min},y_{max}])$：绘制二元函数 $f(x,y)$ 在指定区域内带有等值线的表面图，其中 $x$ 和 $y$ 的区间分别为 $[x_{min},x_{max}]$ 和 $[y_{min},y_{max}]$。
7. $ezsurfc(x,y,z)$：在默认区域内绘制带有等值线的三维参数方程表面图。
8. $ezsurfc(x,y,z,[s_{min},s_{max},t_{min},t_{max}])$ 或 $ezsurfc(x,y,z,[min,max])$ ：在指定区域内绘制带有等值线的三维参数方程表面图。

$ezsurf$ 函数和 $ezsurfc$ 函数都可以进行三维表面图的绘制，但 $ezsurfc$ 函数在绘制三维表面图时还可以进行等值线的绘制。

例：

```matlab
syms t;
syms x;
f1 = x * sin(t);
f2 = x * cos(t);
f3 = t;
ezsurf(f1, f2, f3, [0, 5*pi])
title('无等值线表面图')
```

运行结果：

![](https://gitee.com/RocDe/picture/raw/master/img/20210218213916.png)



```matlab
syms t;
syms x;
f1 = x * sin(t);
f2 = x * cos(t);
f3 = t;
ezsurfc(f1, f2, f3, [0, 5*pi])
title('有无等值线表面图')
```

运行结果：

![](https://gitee.com/RocDe/picture/raw/master/img/20210218214028.png)



## 曲面图绘制

在 MATLAB 中使用 $ezmesh$ 函数和 $ezmeshc$ 函数进行曲面图的绘制，其调用格式如下。

1. $ezmesh(f)$ ：绘制二元函数 $f(x,y)$ 在默认区域的曲面图。
2. $ezmesh(f,[x_{min},x_{max}],[y_{min},y_{max}])$ ：绘制二元函数 $f(x,y)$ 在指定区域的曲面图，其中 $x$ 和 $y$ 的区间分别为 $[x_{min},x_{max}]$ 和 $[y_{min},y_{max}]$。
3. $ezmesh(x,y,z)$ ：在默认区域内绘制三维参数方程的曲面图。
4. $ezmesh(x,y,z,[s_{min},s_{max},t_{min},t_{max}])$ 或 $ezmesh(x,y,z,[min,max])$ ：在指定区域内绘制三维参数方程的曲面图。
5. $ezmeshc(f)$ ：绘制二元函数 $f(x,y)$ 在默认区域内带有等值线的曲面图。
6. $ezmeshc(f,[x_{min},x_{max}],[y_{min},y_{max}])$ ：绘制二元函数 $f(x,y)$ 在指定区域内带有等值线的曲面图，其中 $x$ 和 $y$ 的区间分别为 $[x_{min},x_{max}]$ 和 $[y_{min},y_{max}]$。
7. $ezmeshc(x,y,z)$ ：在默认区域内绘制带有等值线的三维参数方程的曲面图。
8. $ezmeshc(x,y,z,[s_{min},s_{max},t_{min},t_{max}])$ 或 $ezmeshc(x,y,z,[min,max])$ ：在指定区域内绘制带有等值线的三维参数方程的曲面图。

$ezmesh$ 函数和 $ezmeshc$ 函数都可以进行三维曲面的绘制，但 $ezmeshc$ 函数在绘制三维曲面图时还可以进行等值线的绘制。

例：

```matlab
syms x;
syms t;
f = x*sin(t);
ezmesh(f,[-pi,pi]);
title('无等值线曲面图')
```

运行结果：

![](https://gitee.com/RocDe/picture/raw/master/img/20210218214144.png)



```matlab
syms x;
syms t;
f = x*sin(t);
ezmeshc(f,[-pi,pi]);
title('有等值线曲面图')
```

![](https://gitee.com/RocDe/picture/raw/master/img/20210218214219.png)

