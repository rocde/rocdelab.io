---
title: Java 去除空格的方法
date: 2021-02-20 14:10:54
updated: 2021-02-20 14:10:54
type: 
mathjax: false
tags: 
- Java
categories: 
- Java
comments: true
---

# Java 去空格

`trim` 方法

```java
public String trim()
```

去掉首位空格。

`replaceAll` 方法

```java
public String replaceAll(String regex, String replacement)
```

`str.replaceAll(" ","")`

`str.replaceAll("\\s*","")`

