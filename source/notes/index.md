---
title: 札记
date: 2021-02-10 22:18:21
updated: 2021-02-13 22:35:07
type: "notes"
comments: false
---



# 2021 年 3 月

C 语言多行宏使用 do... while

****

32 位系统和 64 位系统的不同在于处理数据的能力、寻址能力（支持的内存不同）和软件兼容性的不同。

# 2021 年 2 月

千金之剑，以之析薪，则不如斧。三代之鼎，以之垦田，则不如耜。

***

Ubuntu20.04创建与删除用户

添加新用户

```bash
sudo adduser <username>
```

 验证新用户账户

```bash
cat /ect/passwd | grep <username>
```

把用户添加到 `sudo` 组

```bash
sudo usermod -aG sudo <username>
```

删除用户

```bash
sudo deluser <username>
```



# 2021 年 1 月

加速C++输入速度，让 `cin` 不超时。

取消 `cin` 的同步，但是此后不能和 `scanf`, `sscanf`, `getchar`,  `fget` 混合使用，不然会出问题。

```cpp
#include <bits/stdc++.h>

using namspace std;

int main(void) {
    ios::sync_with_stdio(false);
    cin.tie(0);
    
    return 0;
}
```

此外，`cout` 尽量少使用 `endl`。

# 2020 年 12 月

