---
title: Java 多线程
date: 2021-02-20 15:55:11
updated: 2021-02-21 14:57:17
type: 
mathjax: false
tags: 
- Java
- multithread
categories:
- Java
	-  multithread
comments: true
---



# 多线程

## 相关概念

### 进程 vs 线程

进程和线程是包含关系，但是多任务既可以由多进程实现，也可以由单进程内的多线程实现，还可以混合多进程 ＋ 多线程。

具体采用哪种方式，要考虑到进程和线程的特点。

<!--more-->

和多线程相比，多进程的缺点在于：

- 创建进程比创建线程开销大，尤其是在 Windows 系统上；
- 进程间通信比线程间通信要慢，因为线程间通信就是读写同一个变量，速度很快。

而多进程的优点在于：

多进程稳定性比多线程高，因为在多进程的情况下，一个进程崩溃不会影响其他进程，而在多线程的情况下，任何一个线程崩溃会直接导致整个进程崩溃。



# Java 多线程

Java 语言内置了多线程支持：一个 Java 程序实际上是一个 JVM 进程，JVM 进程用一个主线程来执行 `main()` 方法，在 `main()` 方法内部，我们又可以启动多个线程。此外，JVM 还有负责垃圾回收的其他工作线程等。

因此，对于大多数 Java 程序来说，我们说多任务，实际上是说如何使用多线程实现多任务。

和单线程相比，多线程编程的特点在于：多线程经常需要读写共享数据，并且需要同步。例如，播放电影时，就必须由一个线程播放视频，另一个线程播放音频，两个线程需要协调运行，否则画面和声音就不同步。因此，多线程编程的复杂度高，调试更困难。

Java 多线程编程的特点又在于：

- 多线程模型是 Java 程序最基本的并发模型；
- 后续读写网络、数据库、Web 开发等都依赖 Java 多线程模型。

## 线程的生命周期

线程在其生命周期会经历各个阶段，下图展示了线程的完整生命周期。

- New：新创建的线程，尚未执行；
- Runnable：运行中的线程，正在执行 `run()` 方法的 Java 线程；
- Blocked：运行中的线程，因为某些操作被阻塞而挂起；
- Waiting：运行中的线程，因为某些操作在等待中；
- Timed Waiting：运行中的线程，因为执行 `sleep()` 方法正在计时等待；
- Terminated：线程已终止，因为 `run()` 方法执行完毕。

详细内容参考 [官方文档](https://docs.oracle.com/javase/8/docs/api/java/lang/Thread.State.html) 

下面两图展示了线程之间状态的转换

![image-20210220161528412](https://gitee.com/RocDe/picture/raw/master/img/20210220161528.png)

![img](https://gitee.com/RocDe/picture/raw/master/img/20210220164353.jpeg)



# 创建线程

## 继承 `Thread` 类，重写 `run()` 方法：

```java
public class Main {
    public static void main(String[] args) {
        Thread t = new MyThread();
        t.start(); // 启动新线程，会调用 run() 方法
    }
}

class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println("start new thread!");
    }
}
```

## 创建 `Thread` 实例时，传入一个 `Runnable` 实例：

```java
public class Main {
    public static void main(String[] args) {
        Thread t = new Thread(new MyRunnable());
        t.start();
    }
}

class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("start new thread!");
    }
}
```

## 通过 `Callable` 和 `Future` 创建线程

```java
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Main implements Callable<String> {

    @Override
    public String call() throws Exception {
        System.out.println("start call()");
        Thread.sleep(2000);
        return "sleep 2s";
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Main m = new Main();
        FutureTask<String> task = new FutureTask<>(m);
        Thread t = new Thread(task);
        t.start();
        System.out.println("start t");
        String res = task.get();
        System.out.println("return \"" + res + "\"");
    }
}
```

## 创建线程的三种方式对比

- 采用实现 `Runnable`、`Callable` 接口的方式创建多线程时，线程类只是实现了 `Runnable` 接口或 `Callable` 接口，还可以继承其他类。
- 使用继承 `Thread` 类的方式创建多线程时，编写简单，如果需要访问当前线程，则无需使用 `Thread.currentThread ()` 方法，直接使用 `this` 即可获得当前线程。



# 线程同步

如果多个线程同时读写共享变量，会出现数据不一致的问题。

例如：

```java
public class Main {
    public static void main(String[] args) throws Exception {
        var add = new AddThread();
        var dec = new DecThread();
        add.start();
        dec.start();
        add.join();
        dec.join();
        System.out.println(Counter.count);
    }
}

class Counter {
    public static int count = 0;
}

class AddThread extends Thread {
    public void run() {
        for (int i=0; i<10000; i++) { Counter.count += 1; }
    }
}

class DecThread extends Thread {
    public void run() {
        for (int i=0; i<10000; i++) { Counter.count -= 1; }
    }
}
```

运行这段代码的结果应该是 `0` 的，但是实际上每次运行的结果都不一样。这是因为对变量进行读取和写入时，结果要正确，必须保证是原子操作。

这里可以使用 `synchronized` 加锁和解锁来保证在同意时刻只有一个线程运行。上面的代码可以改成这样

```java
public class Main {
    public static void main(String[] args) throws Exception {
        var add = new AddThread();
        var dec = new DecThread();
        add.start();
        dec.start();
        add.join();
        dec.join();
        System.out.println(Counter.count);
    }
}

class Counter {
    public static final Object lock = new Object();
    public static int count = 0;
}

class AddThread extends Thread {
    public void run() {
        for (int i=0; i<10000; i++) {
            synchronized(Counter.lock) {
                Counter.count += 1;
            }
        }
    }
}

class DecThread extends Thread {
    public void run() {
        for (int i=0; i<10000; i++) {
            synchronized(Counter.lock) {
                Counter.count -= 1;
            }
        }
    }
}
```

现在运行的结果就是 `0` 了。

注意到代码：

```java
synchronized(Counter.lock) { // 获取锁
    ...
} // 释放锁
```

它表示用 `Counter.lock` 实例作为锁，两个线程在执行各自的 `synchronized(Counter.lock) { ... }` 代码块时，必须先获得锁，才能进入代码块进行。执行结束后，在 `synchronized` 语句块结束会自动释放锁。这样一来，对 `Counter.count` 变量进行读写就不可能同时进行。上述代码无论运行多少次，最终结果都是 0。

使用 `synchronized` 解决了多线程同步访问共享变量的正确性问题。但是，它的缺点是带来了性能下降。因为 `synchronized` 代码块无法并发执行。此外，加锁和解锁需要消耗一定的时间，所以，`synchronized` 会降低程序的执行效率。

我们来概括一下如何使用 `synchronized`：

1. 找出修改共享变量的线程代码块；
2. 选择一个共享实例作为锁；
3. 使用 `synchronized(lockObject) { ... }`。

在使用 `synchronized` 的时候，不必担心抛出异常。因为无论是否有异常，都会在 `synchronized` 结束处正确释放锁：

```java
public void add(int m) {
    synchronized (obj) {
        if (m < 0) {
            throw new RuntimeException();
        }
        this.value += m;
    } // 无论有无异常，都会在此释放锁
}
```

对 JVM 定义的单个原子操作不需要同步。



参考：

[廖雪峰的官方网站](https://www.liaoxuefeng.com/wiki/1252599548343744/1255943750561472) 

[菜鸟教程](https://www.runoob.com/java/java-multithreading.html) 

[Java doc](https://docs.oracle.com/javase/8/docs/api/java/lang/Thread.State.html) 

