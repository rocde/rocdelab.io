---
title: 使用 Hexo + GitLab 搭建自己的博客
date: 2021-02-02 14:18:05
updated: 2021-02-04 19:32:35
type:
tags:
- other
categories:
- tutorials
	- blog
comments: true
---



## 前言

这篇是对自己建站过程的回顾而总结的一篇教程，方便自己以后回顾。当然，由于自己也是接触不久，所以文章中间可能有错误。在此声明，此文仅供参考！

博客有许多第三方平台，也可以自建。我的选择是自建，使用 GitLab + Hexo，操作系统是 Windows10 

<!--more-->

## 环境准备

- [Node.js](https://nodejs.org/en/)  (Node.js 版本需不低于 10.13，官方建议使用 Node.js 12.0 及以上版本)
- [Git](https://git-scm.com/)

Node.js 的网络问题自行解决

这里假定已经做好了前期准备。

## Hexo

详细内容可以参照 [文档 | Hexo](https://hexo.io/zh-cn/docs/) 

### 安装

准备工作做完之后就可使用 npm 安装 Hexo 建议使用 git bash 

```bash
$ npm install -g hexo-cli
```

安装完成后，执行以下命令，Hexo 将在指定文件夹中新建所需文件

```bash
$ hexo init <folder>
$ cd <folder>
$ npm install
```

文件夹的目录会变成这样

```
.
├── _config.yml # 配置文件
├── package.json # 应用程序信息
├── scaffolds # 模板文件夹
├── source # 存放用户资源
|   ├── _drafts # 草稿文件夹
|   └── _posts # 会发布的文章用的文件夹
└── themes # 主题文件夹
```

### 预览站点

如果命令没有报错的话可以尝试运行 `hexo s` 在浏览器中访问 http://localhost:4000/ 

效果应该是这样的，现在的主题是默认主题 landscape 

![](https://gitee.com/RocDe/picture/raw/master/img/20210205004941.png)

至此，个人本地博客就已经搭建好了。

创建新文章或者新页面

```bash
$ hexo new post <title>
```

此命令会在 `source/_posts` 文件夹里生成一个 Markdown 文件我们只需要编辑这个文件即可。

## GitLab

在 [GitLab](https://about.gitlab.com/) 上注册一个账号，再新建一个名为 `<yourname>.gitlab.io` 的空白仓库。

注：如果想公开访问的话就设置成公开项目，否则设为私有项目。

### Git 设置

主要是设置用户名和邮箱，在Git Bash中运行以下下命令

```bash
git config --global user.name "你的GitLab用户名"
git config --global user.email "你的GitLab注册邮箱"
```

生成 ssh 密钥文件

```bash
ssh-keygen -t rsa -C "你的GitLab注册邮箱"
```

运行后会提示输入密码，我们回车就行。

然后打开用户文件夹下的 .ssh 文件夹，用记事本打开 `id_rsa.pub` 复制全部内容。

打开此网址 https://gitlab.com/-/profile/keys 

![](https://gitee.com/RocDe/picture/raw/master/img/20210205014614.png)

复制的内容粘贴到 Key 里面，然后随便起个名添加添加即可。

然后，我们需要在我们的博客目录下添加一个名为 `.gitlab-ci.yml` 的文件，内容如下

```
image: node:10.15.3

cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - test -e package.json && npm install
  - hexo generate

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  only:
    - master
```

注：可以参考官方提供的最新内容 [.gitlab-ci.yml](https://gitlab.com/pages/hexo/-/blob/master/.gitlab-ci.yml) 

### Hexo 设置

在博客根目录里的设置文件 `_config.yml` 里找到 url 字段，设置为 https://\<yourname>.gitlab.io 

其他的设置我以后再总结。

### 部署到 GitLab 

在博客文件夹打开 Git Bash 运行以下命令

```bash
git init
git remote add origin git@gitlab.com:<yourname>/<yourname>.gitlab.io.git
git add .
git commit -m "Initial commit"
git branch master
git checkout master
git branch -d main
git push -u origin master
```

注：要在 `<yourname>` 填入你的 GitLab 用户名

回到 GitLab，查看项目侧栏的 CI / CD ，如果 Stage 变为两个绿色的勾的时候说明已经部署完成啦

![](https://gitee.com/RocDe/picture/raw/master/img/20210205022820.png)

访问 https://\<yourname>.gitlab.io 就可以看到博客页面了。

注：要在 `<yourname>` 填入你的 GitLab 用户名

至此，就已经完成了个人博客的搭建与部署。