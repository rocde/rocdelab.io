---
title: 在 Ubuntu 上部署饥荒服务器
date: 2021-02-10 13:19:10
updated: 2021-02-10 13:19:10
type: 
mathjax: false
tags: 
- game
categories:
- game 
comments: true
---

# 在 Ubuntu 上部署饥荒服务器

1. 安装依赖

   64 bit

   ```bash
   sudo apt-get install libstdc++6:i386 libgcc1:i386 libcurl4-gnutls-dev:i386
   ```

   32 bit

   ```bash
   sudo apt-get install libstdc++6 libgcc1 libcurl4-gnutls-dev
   ```

   如果有报错就先运行一下命令

   ```bash
   sudo dpkg --add-architecture i386
   sudo apt-get update
   ```

   <!--more-->

2. 安装 steamcmd

   缩减后的命令

   ```bash
   mkdir -p ~/steamcmd/
   cd ~/steamcmd/
   wget "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz"
   tar -xvzf steamcmd_linux.tar.gz
   ```

3. 配置与下载游戏服务

   访问这个网站 https://accounts.klei.com/login 使用 Steam 账号登录

   ![image-20210209071523920](https://gitee.com/RocDe/picture/raw/master/img/20210209071524.png)

   添加新服务器。下载配置文件
   
   把 `MyDediServe` 文件夹放到 `~/.klei/DoNotStarveTogether/` 里。
   
4. 新建运行服务脚本

    `~/run_dedicated_servers.sh` 

   ```bash
   #!/bin/bash
   
   steamcmd_dir="$HOME/steamcmd"
   install_dir="$HOME/dontstarvetogether_dedicated_server"
   cluster_name="MyDediServer"
   dontstarve_dir="$HOME/.klei/DoNotStarveTogether"
   
   function fail()
   {
   	echo Error: "$@" >&2
   	exit 1
   }
   
   function check_for_file()
   {
   	if [ ! -e "$1" ]; then
   		fail "Missing file: $1"
   	fi
   }
   
   cd "$steamcmd_dir" || fail "Missing $steamcmd_dir directory!"
   
   check_for_file "steamcmd.sh"
   check_for_file "$dontstarve_dir/$cluster_name/cluster.ini"
   check_for_file "$dontstarve_dir/$cluster_name/cluster_token.txt"
   check_for_file "$dontstarve_dir/$cluster_name/Master/server.ini"
   check_for_file "$dontstarve_dir/$cluster_name/Caves/server.ini"
   
   ./steamcmd.sh +force_install_dir "$install_dir" +login anonymous +app_update 343050 validate +quit
   
   check_for_file "$install_dir/bin"
   
   cd "$install_dir/bin" || fail
   
   run_shared=(./dontstarve_dedicated_server_nullrenderer)
   run_shared+=(-console)
   run_shared+=(-cluster "$cluster_name")
   run_shared+=(-monitor_parent_process $$)
   
   "${run_shared[@]}" -shard Caves  | sed 's/^/Caves:  /' &
   "${run_shared[@]}" -shard Master | sed 's/^/Master: /'
   ```

5. 设置脚本可执行权限

   ```bash
   chmod u+x ~/run_dedicated_servers.sh
   ```

6. 运行脚本启动服务

   ```bash
   ~/run_dedicated_servers.sh
   ```

7. 部分服务器指令

    ```bash
    -console -conf_dir ---服务器参数
    
    c_connect("", 10999, "000") ---IP ，端口，密码 服务器直连
    
    TheNet:Kick(userid) ---踢出用户ID为“userid”的玩家
    
    TheNet:Ban(userid) ---禁止用户ID为“userid”的玩家加入
    
    c_save() ---立即保存当前世界(一般会在每天早上自动保存)
    
    c_reset(true|false) ---true保存并重新加载世界;false不保存直接重新加载当前世界
    
    c_regenerateworld() ---重置世界
    
    c_shutdown(true|false) ---true保存并关闭当前世界; false不保存直接关闭当前世界
    
    TheNet:SetAllowIncomingConnections(true|false) ---true允许他人加入;false阻止任何人加入
    
    c_announce("内容") ---发布 公告
    
    TheWorld.net.components.clock:OnUpdate(16*30*60) 跳过时间 X为多少天
    
    TheWorld:PushEvent("ms_nextphase") ---跳过时间阶段(季节)
    
    c_rollback(X)---回档x天 x为你要回档的天数(1-5)
    
    TheWorld:PushEvent("ms_forceprecipitation")---开始下雨
    
    TheWorld:PushEvent("ms_forceprecipitation", false)---雨停了
    
    c_listallplayers() ---列出所有玩家的用户名和玩家号码
    
    Get a certain player
    
    AllPlayers[number]
    
    c_move(AllPlayers[number]) ---移动一个玩家，将玩家移动到光标位置
    
    AllPlayers[number]:PushEvent('death') ---杀死一个玩家，杀死玩家
    
    AllPlayers[number]:PushEvent('respawnfromghost') ---复活的玩家
    
    c_goto(AllPlayers[number]) ---传送到指定玩家身边
    
    TheInput:GetWorldEntityUnderMouse():Remove() ---删除鼠标指向物品
    
    c_select() c_sel():Remove()---按下回车键后，它会删除鼠标下的项。在专用服务器上使用二次命令，第一命令将不工作
    ```

    

