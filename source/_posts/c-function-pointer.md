---
title: C函数指针
date: 2021-02-08 23:19:44
updated: 2021-02-08 23:19:44
type:
tags:
- C 
- pointer 
categories:
- C 
	- pointer
comments: true
---



## 函数指针初探

测试环境 Ubuntu20.04 64位，gcc 版本 9.3.0

```c
#include<stdio.h>

void fun(void){}

int main(void){
    int i = 10;
    int *p = &i;
    printf("p: %p\n", p);
	printf("fun: %p\n", fun);
    printf("main: %p\n", main);
	return 0;
}
```

<!--more-->

运行结果：

```
p: 0x7ffe3f89511c
fun: 0x561adb583169
main: 0x561adb583174
```

通过这段代码，我们可以感受到函数名与指针都表示着一段地址，而且函数好像是放在一块的（地址值比较接近）。

我们再编译以下代码。

```c
#include<stdio.h>

void fun(void){}

int main(void){
    int *p;
    p = fun;
    return 0;
}
```

编译器的报错如下：

![image-20210208234554519](https://gitee.com/RocDe/picture/raw/master/img/20210208234554.png)

原来这个 `fun` 的类型是 `void (*)(void)`。

这么说来，我们应该可以使用一个类型为 `void (*)(void)` 的东西接受 `fun` 的地址。

```c
#include<stdio.h>

void fun(void){}

int main(void){
    void (*pFun)(void) = fun;
    printf("pFun: %p\n", pFun);
    printf("fun: %p\n", fun);
    return 0;
}
```

运行结果：

```
pFun: 0x562450909149
fun: 0x562450909149
```

好像确实是这么回事诶。

那我们是不是可以通过 `pFun` 调用 `fun`。

```c
#include<stdio.h>

void fun(void) {
    printf("In fun()\n");
}

int main(void){
    // int *p;
    void (*pFun)(void) = fun;
    printf("pFun: %p\n", pFun);
    printf("fun: %p\n", fun);
    fun();
    (*pFun)();
    return 0;
}
```

运行结果：

```
pFun: 0x559dfb11b169
fun: 0x559dfb11b169
In fun()
In fun()
```

确实如此！也就是说 `pFun` 其实是指向函数 `fun` 的指针。

## 函数指针的特点

1. 与普通指针不同，函数指针指向代码，而不是数据。
2. 与普通指针不同，我们不会使用函数指针去分配内存。
3. 函数的名称也可以用于获取函数的地址。`fun` 等价于 `&fun`。
4. 与普通指针一样，可以有一个函数指针数组。
5. 与普通指针一样，函数指针可以作为参数传递，也可以作为函数返回值。

## 函数指针的使用

### 重构代码

有时候我们会写这样的代码

```c
#include<stdio.h>

void add(int a, int b){
    printf("a + b = %d\n", a+b);
}

void sub(int a, int b){
    printf("a - b = %d\n", a-b);
}

void mult(int a, int b){
    printf("a * b = %d\n", a*b);
}

int main(void){
    int op, a = 2, b = 3;

    printf("Enter Choice: 0 for add, 1 for subtract and 2 for multiply\n");
    scanf("%d", &op);
    switch(op){
        case 0: add(a, b);break;
        case 1: sub(a, b);break;
        case 2: mult(a, b);break;
    }

    return 0;
}
```

也可以这样写

```c
#include<stdio.h>

void add(int a, int b){
    printf("a + b = %d\n", a+b);
}

void sub(int a, int b){
    printf("a - b = %d\n", a-b);
}

void mult(int a, int b){
    printf("a * b = %d\n", a*b);
}

int main(void){
    int op, a = 2, b = 3;
    void (*fun_arr[])(int, int) = {add, sub, mult};

    printf("Enter Choice: 0 for add, 1 for subtract and 2 for multiply\n");
    scanf("%d", &op);
    
    if(op >= 0 && op <= 2){
        (*fun_arr[op])(a,b);
    }

    return 0;
}
```

运行示例：

```
Enter Choice: 0 for add, 1 for subtract and 2 for multiply
2
a * b = 6
```

可以使用函数指针（数组）重构代码。

### 作为传入函数的参数

```c
#include<stdio.h>

void add(int a, int b){
    printf("%d + %d = %d\n", a, b, a+b);
}

void sub(int a, int b){
    printf("%d - %d = %d\n", a, b, a-b);
}

void call(void (*fun)(int, int)){
    int a = 2, b = 3;
    fun(a, b);
}

int main(void){
    call(add);
    call(sub);

    return 0;
}
```

运行结果：

```
2 + 3 = 5
2 - 3 = -1
```

这个特性非常有趣，我们可以利用这个特性减少代码的冗余。

在C的标准库中也有这样使用的身影，例如 `stdlib.h` 里的 [`qsort` 函数](http://www.cplusplus.com/reference/cstdlib/qsort/)。我们可以使用这个函数升序或降序排列各种数组的元素，包括一些我们自定义的结构体数组等。

函数原型：

```c
void qsort (void* base, size_t num, size_t size,
            int (*compar)(const void*,const void*));
```

偷懒贴个文档中的使用例子：

```c
/* qsort example */
#include <stdio.h>      /* printf */
#include <stdlib.h>     /* qsort */

int values[] = { 40, 10, 100, 90, 20, 25 };

int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

int main ()
{
  int n;
  qsort (values, 6, sizeof(int), compare);
  for (n=0; n<6; n++)
     printf ("%d ",values[n]);
  return 0;
}
```

输出：

```
10 20 25 40 90 100
```

也可以仿照这个函数写出一些低冗余，可以适用于各种数据类型的函数。比如下面这个例子

```c
#include <stdio.h> 
#include <stdbool.h> 

bool compare (const void * a, const void * b) { 
	return ( *(int*)a == *(int*)b ); 
} 

int search(void *arr, int arr_size, int ele_size, void *x,
           bool compare (const void * , const void *)) { 

	char *ptr = (char *)arr; 

	int i; 
	for (i=0; i < arr_size; i++) 
		if (compare(ptr + i * ele_size, x)) 
		return i; 

	return -1; 
} 

int main() { 
	int arr[] = {2, 5, 7, 90, 70}; 
	int n = sizeof(arr)/sizeof(arr[0]); 
	int x = 7; 
	printf ("Returned index is %d ", search(arr, n, sizeof(int),
                                            &x, compare)); 
	return 0; 
} 

```

输出：

```
Returned index is 2
```

持续更新中...

参考文章：

[Function Pointer in C](https://www.geeksforgeeks.org/function-pointer-in-c/) 

[Function pointer in C, a detail guide](https://aticleworld.com/use-of-function-pointer-in-c/) 

https://www.cs.rit.edu/~ats/books/ooc.pdf 